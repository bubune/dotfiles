#!/bin/sh

# install prg
sudo apt-get install vim
sudo apt-get install git
sudo apt-get install i3
sudo apt-get install feh

# Download vim.plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
