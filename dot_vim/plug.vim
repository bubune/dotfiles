if exists("g:plug_local_repo")
	let g:plug_url_format = g:plug_local_repo.'/%s'
endif

call plug#begin('~/.vim/plugged')

"Fugitive is the premier Vim plugin for Git. Or maybe it's the premier Git
"plugin for Vim? Either way, it's "so awesome, it should be illegal". That's
"why it's called Fugitive.
Plug 'tpope/vim-fugitive'

"Think of sensible.vim as one step above 'nocompatible' mode: a universal set
"of defaults that (hopefully) everyone can agree on.
Plug 'tpope/vim-sensible'

"Surround.vim is all about "surroundings": parentheses, brackets, quotes, XML
"tags, and more. The plugin provides mappings to easily delete, change and add
"such surroundings in pairs.
Plug 'tpope/vim-surround'

"Comment stuff out. Use gcc to comment out a line (takes a count), gc to
"comment out the target of a motion (for example, gcap to comment out a
"paragraph), gc in visual mode to comment out the selection, and gc in
"operator pending mode to target a comment.
Plug 'tpope/vim-commentary'

"Vim sugar for the UNIX shell commands that need it the most.
Plug 'tpope/vim-eunuch'

"easily search for, substitute, and abbreviate multiple variants of a word
Plug 'tpope/vim-abolish'

"match-up is a plugin that lets you highlight, navigate, and operate on sets
"of matching text. It extends vim's % key to language-specific words instead
"of just single characters.
Plug 'andymass/vim-matchup'

"Gundo.vim is Vim plugin to visualize your Vim undo tree.
Plug 'sjl/gundo.vim'

"Ease your git workflow within vim.
"From a very single vim buffer, you can perform main git operations in few key
"press.
Plug 'jreybert/vimagit'

"A Vim plugin which shows a git diff in the sign column. It shows which lines
"have been added, modified, or removed. You can also preview, stage, and undo
"individual hunks; and stage partial hunks. The plugin also provides a hunk
"text object.
Plug 'airblade/vim-gitgutter'

nmap <leader>hp <Plug>(GitGutterPrevHunk)
nmap <leader>hn <Plug>(GitGutterNextHunk)

"git-messenger.vim is a Vim/Neovim plugin to reveal the hidden message under
"the cursor quickly. It shows the last commit message under the cursor in popup
"window.
Plug 'rhysd/git-messenger.vim'

"Lean & mean status/tabline for vim that's light as air.
Plug 'vim-airline/vim-airline'
let g:airline_powerline_fonts = 1
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#branch#displayed_head_limit = 20
" do not show gitgutter summary
let g:airline#extensions#hunks#enabled = 0
" skip file encoding section for some expected values
let g:airline#parts#ffenc#skip_expected_string='utf-8[unix]'
"
" The `unique_tail` algorithm will display the tail of the filename, unless
" there is another file of the same name, in which it will display it along
" with the containing parent directory.
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
" let g:airline#extensions#tabline#formatter = 'unique_tail'
" let g:airline#extensions#tabline#formatter = 'short'
 " let g:airline#extensions#tabline#formatter = 'short_path'
let g:airline#extensions#tabline#fnamecollapse = 1
" let g:airline_stl_path_style = 'short'
" let g:airline_inactive_collapse=1
let g:airline_skip_empty_sections = 1
let g:airline#extensions#wordcount#enabled = 0

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
let g:airline_symbols.colnr = ' '
let g:airline_symbols.linenr = ' '
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.notexists = '  '
let g:airline_symbols.branch = ''
let g:airline_left_sep = ''
" let g:airline_left_sep = ''
let g:airline_right_sep = ''
" let g:airline_right_sep = ''

let g:airline#extensions#branch#format = 'CustomBranchName'
function! CustomBranchName(name)
    let toks = split(a:name, '/')
    if len(toks) == 1
        let extract = a:name
    elseif len(toks) == 2 || len(toks) == 3
        let extract = toks[1]
    endif
    let extract = substitute(extract, 'jerome.reybert[.-]', '', '')
    return extract
endfunction


" vim-qf—short for vim-quickfix—is a growing collection of settings, commands
" and mappings put together to make working with the location list/window and
" the quickfix list/window smoother.
Plug 'romainl/vim-qf'

" The linediff plugin provides a simple command, :Linediff, which is used to diff
" two separate blocks of text.
Plug 'AndrewRadev/linediff.vim'

"This plugin adds mappings and a :Mark command to highlight several words in
"different colors simultaneously
Plug 'inkarkat/vim-mark'
"vim-mark dependency
Plug 'inkarkat/vim-ingo-library'
let g:mwDefaultHighlightingPalette = 'maximum'
let g:mwAutoLoadMarks = 1
let g:mwAutoSaveMarks = 1

"fzf is a general-purpose command-line fuzzy finder.
"fzf latest is buggy, see https://github.com/junegunn/fzf.vim/issues/1301
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
nnoremap <leader><leader> :gFiles<CR>
nnoremap <leader>l        :files<CR>
nnoremap <leader>L        :files %:p:h<CR>
nnoremap <leader>o        :buffers<CR>
nnoremap <leader>t        :tags<CR>
nnoremap <leader>g        :rg! <C-R><C-W><CR>

"file-line is a plugin for Vim that enables opening a file in a given line.
"When you open a file:line, for instance when coping and pasting from an error
"from your compiler vim tries to open a file with a colon in its name.
"vim index.html:20
"vim app/models/user.rb:1337
Plug 'lervag/file-line'
let g:file_line_crosshairs=0

"Make the yanked region apparent!
Plug 'machakann/vim-highlightedyank'
let g:highlightedyank_highlight_duration = 500
if !exists('##TextYankPost')
	map y <Plug>(highlightedyank)
endif

if ( ( has('nvim') && has('timers') && has('nvim-0.2.0') ) ||
    \ ( has('timers') && exists('*job_start') && exists('*ch_close_in') ) )
	"ALE (Asynchronous Lint Engine) is a plugin providing linting (syntax
	"checking and semantic errors) in NeoVim 0.2.0+ and Vim 8 while you edit
	"your text files, and acts as a Vim Language Server Protocol client.
	Plug 'dense-analysis/ale'
else
	"Syntastic is a syntax checking plugin for Vim created by Martin Grenfell.
	"It runs files through external syntax checkers and displays any resulting
	"errors to the user.
	Plug 'vim-syntastic/syntastic'
endif

"Collection of awesome color schemes for Vim, merged for quick use.
Plug 'rafi/awesome-vim-colorschemes'

"gives every pair of brackets a unique color, so it's easy to identify which
"brackets belong to each other
Plug 'frazrepo/vim-rainbow'

"The NERDTree is a file system explorer for the Vim editor. Using this plugin,
"users can visually browse complex directory hierarchies, quickly open files
"for reading or editing, and perform basic file system operations.
Plug 'preservim/nerdtree'

" This plugin formats your code with specific coding style using clang-format
Plug 'rhysd/vim-clang-format'

let g:clang_format#detect_style_file = 1
" disable "automatic format inserted lines on leaving insert mode"
" this mode is not satisfactory, because it can format more lines
" than you want
let g:clang_format#auto_format_on_insert_leave = 0
" "-fallback-style=none" option is added on executing clang-format
let g:clang_format#enable_fallback_style = 0
" 'formatexpr' option is set automatically in c, cpp and objc codes
let g:clang_format#auto_formatexpr = 1
" automatically formats a current buffer on saving the buffer
let g:clang_format#auto_format = 1
" only formats modified lines is the file is tracked in git.
let g:clang_format#auto_format_git_diff = 1
let g:clang_format#auto_format_git_diff_fallback = 'file'

" Following functions and autocmd allows to use clang_format
" to format lines of code using ranges and motions with =
function! EqualMotion(type)
    call clang_format#replace(line("'["), line("']"))
endfunction
function! EqualRange() range
    call clang_format#replace(a:firstline, a:lastline)
endfunction
autocmd FileType c,cpp,objc,java,javascript,typescript,proto,arduino
	\ if !clang_format#is_invalid() |
	\     nmap <silent> == :call clang_format#replace(line("."), line("."))<CR>|
	\     vmap <silent> = :call EqualRange()<CR>|
	\     nmap <silent> = :set opfunc=EqualMotion<CR>g@|
	\ endif

"Multiple cursors plugin for vim/neovim
if has('nvim') || v:version >= 800
	Plug 'mg979/vim-visual-multi'
endif

" A common mapping to exit insert mode is "j" followed by "k", though that
" might cause trouble in some languages. Maybe it's more reliable to map them
" *together* using vim-arpeggio
Plug 'kana/vim-arpeggio'

" Plug 'bronson/vim-visual-star-search'
" Waiting for plugin
vnoremap * y/\V<C-R>=escape(@",'/\')<CR><CR>

" Initialize plugin system
call plug#end()

"call arpeggio#load()
"Arpeggio inoremap jk  <Esc>
"colorscheme apprentice

" function! CountSelectedLines()
" 	" execute "normal! <c-g>g"
" 	" return line("'>") - line("'<") + 1
" 	return getpos("'>")[1] - getpos("'<")[1] + 1
" endfunction

" function! MyPlugin(...)
" 	let builder = a:1
" 	call builder.add_section('airline_section_z', '%{CountSelectedLines()}')
" 	" let w:airline_section_z = '%{CountSelectedLines()}'
" 	" let g:airline_variable_referenced_in_statusline = 'foo'
" endfunction
" " call airline#add_inactive_statusline_func('MyPlugin')
" call airline#add_statusline_func('MyPlugin')


